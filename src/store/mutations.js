/**
 * mutations
 * 修改状态，增删改查
 */
import { PLUS_QNUM, MINUS_QNUM, RECORD_ANWSER, ALL_QA } from './mutation-types';
import Vue from 'vue';

export default {
	[PLUS_QNUM](state){
		state.curqId++;
	}, 
	[MINUS_QNUM](state){
		state.curqId--;
	}, 
	[RECORD_ANWSER](state,payload){
		state.userAnwser.push(payload);
	},
	[ALL_QA](state,payload){
		state.paper = payload;
	}
}