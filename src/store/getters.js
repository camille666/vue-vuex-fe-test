/**
 * getters
 * 共享方法
 * 获取从state派生的standAnwserArr状态
 */
export const getterFilterAnwser = state => {
	const standAnwserArr = [];
	state.userAnwser.forEach(function(ele,index){
		standAnwserArr.push(ele['qAnwser']['result']);
	});
	return standAnwserArr;
}