/*
 * 共享状态
 * curqId，用于存储修改的图书位置
 * paper，用于存储所有题目信息
 * userAnwser，用户的答案
 */
export default {
	curqId: 1,
	paper: [],
	userAnwser: []
}