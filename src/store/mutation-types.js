/**
 * mutaion-types
 * 定义全局共享的mutation类型
 * 采用常量大写方式
 */
export const PLUS_QNUM = 'PLUS_QNUM';
export const MINUS_QNUM = 'MINUS_QNUM';
export const RECORD_ANWSER = 'RECORD_ANWSER';
export const ALL_QA = "ALL_QA";

