/**
 * actions
 * 同步或者异步，提交mutation
 */
import { PLUS_QNUM, MINUS_QNUM, RECORD_ANWSER, ALL_QA } from './mutation-types';
import axios from 'axios';

function getData(contextCommit,mutationType,apiUrl){
	axios.get(apiUrl).then((response)=>{
		contextCommit(mutationType, response.data);
	}).catch((error)=>{
		//这里写一个无数据兼容
		console.log(error);
	})
}

export default {
	actionNextStep({commit}, anwser){
		commit('RECORD_ANWSER', anwser);
		if(state.curqId < state.paper.length){
			commit('PLUS_QNUM');
		}
	},
	actionLastStep({commit}){
		commit('MINUS_QNUM');
	},
	actionAllQa({commit}, payload){
		getData(commit,'ALL_QA','../../ajax/question.json')
	}
}