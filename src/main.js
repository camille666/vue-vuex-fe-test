import Vue from 'vue'
import Pcom from './pcom.vue'
import store from './store/store.js'

var vm = new Vue({
  el: '#J_fe_test',
  store,
  components: { 
  	'p-com': Pcom
  }
})