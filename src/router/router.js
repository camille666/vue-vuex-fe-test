import Pcom from '../pcom.vue';

export default {
	path:'/',
	component: Pcom,
	children:[
	{
		path:'',
		component: r => require.ensure([],() => r(require('../page/home.vue')))
	},
	{
		path:'/result',
		component: r => require.ensure([],() => r(require('../page/result.vue')))
	},{
		path:'/analyse',
		component: r => require.ensure([],() => r(require('../page/analyse.vue')))
	}]
}